import { FunctionComponent } from "~/lib/react/types"

export const assembleComponent = <Props, ViewModel>(
    useViewModel: (props: Props) => ViewModel, 
    Component: FunctionComponent<ViewModel>,
): FunctionComponent<Props> => {
    const ComponentWithInjectedViewModel: FunctionComponent<Props> = (props) => {
        const viewModel = useViewModel(props)
        
        return <Component {...viewModel} />
    }

    return ComponentWithInjectedViewModel
}