import { Router } from "~/lib/router/Router"
import Hello from "~/pages/Hello"

export const App = () => (
    <Router>
        <Hello initialCounter={42}/>
    </Router>
)
