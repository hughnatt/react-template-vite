import { assembleComponent } from "../assembly"
import { useViewModel } from "./ViewModel/useViewModel"
import { View } from "./View/View"

export default assembleComponent(useViewModel, View)