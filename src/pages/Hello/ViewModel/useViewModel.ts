import { useNavigate } from "react-router"
import { useState } from "~/lib/react/useState"

import { Props } from "../Props"
import { ViewModel } from "./ViewModel"

export const useViewModel = (props: Props) => {
    const navigate = useNavigate()

    const [viewModel, setViewModel] = useState<ViewModel>({
        counter: props.initialCounter ?? 0,
        onButtonClicked: () => {
            setViewModel(viewModel => ({
                ...viewModel,
                counter: viewModel.counter + 1,
            }))
        },
        onAnotherButtonClicked: () => {
            navigate("/test")
        },
    })

    return viewModel
}