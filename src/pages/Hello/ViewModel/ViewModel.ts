export type ViewModel = {
    counter: number,
    onButtonClicked: () => void,
    onAnotherButtonClicked: () => void,
}