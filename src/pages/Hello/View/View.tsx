import { ViewModel } from "../ViewModel/ViewModel"
import { Button } from "~/components/Button/Button"

export const View = ({
    onButtonClicked,
    onAnotherButtonClicked,
    counter,
}: ViewModel) => {
    return (
        <>
            <Button handleClick={onButtonClicked}>
                {counter}
            </Button>
            <Button handleClick={onAnotherButtonClicked}>
                Click here too
            </Button>
        </>
    )
}

