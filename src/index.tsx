import { render } from "~/lib/react/render"
import { App } from "./pages/App"
import "./index.css"

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
render(<App />, document.getElementById("root")!)
