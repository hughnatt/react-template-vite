import { Children } from "~/lib/react/types"

interface Props {
    handleClick: () => void,
    children?: Children,
}

export const Button = ({
    handleClick,
    children,
}: Props) => {
    return (
        <button onClick={handleClick}>
            {children}
        </button>
    )
}