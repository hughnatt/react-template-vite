import { VNode, FunctionComponent } from "preact"

export type Child = VNode | string | number | boolean | undefined | null;
export type Children = Child | Child[] | Children[];
export type { FunctionComponent as FunctionComponent }
